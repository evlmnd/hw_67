import React, {Component} from 'react';
import {connect} from 'react-redux';
import './Password.css';

class Password extends Component {

    render() {
        const code = '*'.repeat(this.props.code.length);

        const buttons = [];
        for (let i = 1; i < 10; i++) {
            buttons.push(<button onClick={() => this.props.addNumber(i)} key={i}>{i}</button>)
        }

        return (
            <div className="Password">
                <p className={['Code', (this.props.isCodeValid ? 'Green' : '')].join(' ')}>{code}</p>
                <div className="Buttons-Box">
                    {buttons}
                    <button onClick={this.props.removeNumber}>&lt;</button>
                    <button onClick={() => this.props.addNumber(0)}>0</button>
                    <button onClick={this.props.enterCode}>enter</button>
                </div>
                {this.props.isCodeValid ? <p className="Message">Welcome</p> : null}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        code: state.code,
        isCodeValid: state.isCodeValid
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addNumber: number => dispatch({type: 'ADD_NUMBER', number}),
        removeNumber: () => dispatch({type: 'REMOVE_NUMBER'}),
        enterCode: () => dispatch({type: 'ENTER_CODE'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Password);
