const rightPassword = '1234';
const initialState = {
    code: '',
    isCodeValid: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_NUMBER':
            if (state.code.length <= 3) {
                return {
                    ...state,
                    code: state.code + action.number
                };
            } else {
                return state;
            }
        case 'REMOVE_NUMBER':
            const newCode = state.code.substr(0, state.code.length - 1);
            return {
                ...state,
                code: newCode
            };
        case 'ENTER_CODE':
            if (state.code === rightPassword) {
                return {
                    ...state,
                    isCodeValid: true
                }
            } else return {
                ...state,
                isCodeValid: false  // наверное это не нужное действие, но пусть будет
            };
        default:
            return state;
    }
};

export default reducer;